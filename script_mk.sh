DOMAIN="dominiocli.com.io"
CLIENTE="name"
HOSTNAME="hostnameDB"
S3USER="USERIAMAWS"
S3KEY="KEYIAMAWS"
S3SSLURL="s3://bucketname/ssl/letsencrypt.tar.gz"
CRONCOMAND="1 1 * * * root aws s3 cp $S3SSLURL /etc/. && cd /etc/ && tar -xvzf letsencrypt.tar.gz"

apt update
apt install -y git ruby
codedeploy_git_url='https://github.com/aws/aws-codedeploy-agent.git'
git clone "$codedeploy_git_url"
sudo gem install bundler
sudo mv aws-codedeploy-agent /opt/codedeploy-agent
cd /opt/codedeploy-agent
bundle install --system

# Setup permissions
sudo chown -R root.root /opt/codedeploy-agent
sudo chmod 644 /opt/codedeploy-agent/conf/codedeployagent.yml
sudo chmod 755 /opt/codedeploy-agent/init.d/codedeploy-agent
sudo chmod 644 /opt/codedeploy-agent/init.d/codedeploy-agent.service

# Create symlink to match ./install setup
sudo mkdir -p /etc/codedeploy-agent
sudo ln -s /opt/codedeploy-agent/conf /etc/codedeploy-agent/conf

# Move init scripts
sudo mv /opt/codedeploy-agent/init.d/codedeploy-agent /etc/init.d/codedeploy-agent
sudo mv /opt/codedeploy-agent/init.d/codedeploy-agent.service \
        /lib/systemd/system/codedeploy-agent.service

# Delete RHEL init info since this is for Ubuntu Bionic
sudo sed -i.bak '2,8d' /etc/init.d/codedeploy-agent && \
sudo rm -f /etc/init.d/codedeploy-agent.bak

# Enable init.d scripts to start at boot
sudo /etc/init.d/codedeploy-agent start && echo ''
sudo /usr/sbin/update-rc.d codedeploy-agent defaults
sudo /usr/sbin/update-rc.d codedeploy-agent enable

# Cleanup
files=(.git
    CODE_OF_CONDUCT.md
    CONTRIBUTING.md
    Gemfile.lock
    LICENSE
    NOTICE
    README.md
    Rakefile
    buildspec-agent-rake.yml
    coverage
    deployment
    features
    spec
    test)

cd /opt/codedeploy-agent

for f in "${files[@]}";do
    sudo rm -rf "$f"
done

##############
# Instalando nginx
sudo apt install nginx zip unzip awscli -y
sudo wget -c https://gitlab.com/palcar24/arch_gen/raw/master/config && sudo mv /root/.aws/.
sudo wget -c https://gitlab.com/palcar24/arch_gen/raw/master/credentials && mv /root/.aws/.
sudo aws s3 cp $S3SSLURL /etc/. && sudo cd /etc/ && sudo tar -xvzf letsencrypt.tar.gz

sudo wget -c https://gitlab.com/palcar24/arch_gen/raw/master/site_nginx_http_https
sudo sed 's/domain.com/$DOMAIN/' site_nginx_http_https > site_nginx_http_https.out
sudo mv site_nginx_http_https.out /etc/nginx/sites-available/default

## Hay que cambiarle el dominio
sudo wget -c https://gitlab.com/palcar24/arch_gen/raw/master/site_nginx_infra
sudo sed 's/domain.com/$DOMAIN/' site_nginx_infra > site_nginx_infra.out
sudo mv site_nginx_infra /etc/nginx/sites-available/infra
sudo cd /etc/nginx/sites-enable/ && sudo ln -s ../site-available/infra
sudo mkdir /var/www/infra && sudo cd /var/www/infra && wget -c https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-all-languages.zip
sudo unzip phpMyAdmin-4.9.0.1-all-languages.zip && sudo mv phpMyAdmin-4.9.0.1-all-languages/* . && sudo  mv phpMyAdmin-4.9.0.1-all-languages/[a-z]* -R .
sudo wget -c https://gitlab.com/palcar24/arch_gen/raw/master/config.inc.php

sudo sed 's/CLIENTE/$CLIENTE/' config.inc.php > config.inc.php.out
sudo sed 's/HOSTNAME/$HOSTNAME/' config.inc.php.out > config.inc.php.out2
sudo mv config.inc.php.out2 /var/www/infra/.
sudo service nginx restart
